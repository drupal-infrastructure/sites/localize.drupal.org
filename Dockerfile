FROM registry.gitlab.com/drupal-infrastructure/drupal-base-images/drupal-php81-base/drupal-php81-base:latest@sha256:31f3dffdde16b4fa2de112475a3a5fdafd5a0ddaddf767fd310ef31c3fce5732 as base

ENV OPCACHE_MEMORY_CONSUMPTION 256
ENV OPCACHE_REVALIDATE_FREQ 600
ENV PHP_MEMORY_LIMIT 1024M
ENV COMPOSER_CACHE_DIR=/dev/null

USER 0
WORKDIR /app

FROM base AS build

# Copy site code
COPY . .

# Install composer dependencies.
RUN composer install --no-ansi --no-dev --no-interaction --no-progress --prefer-dist --optimize-autoloader && \
    mkdir -pv web/sites/default/files && \
    chown -R 1001:0 web/sites/default/files && \
    chmod -R 770 web/sites/default/files && \
    find . -type d -name ".git" | xargs rm -rf


FROM base AS web

ENV PATH "/app/vendor/bin:$PATH"
WORKDIR /app

COPY --from=build /app /app

USER 1001

EXPOSE 8080

CMD ["httpd", "-D", "FOREGROUND"]
