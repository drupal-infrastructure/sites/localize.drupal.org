#!/usr/bin/env bash

# Install from config folder.
ddev drush -y sql:drop
ddev drush -v -y site:install --existing-config --account-pass=admin
ddev drush -y config:import

# These files are downloaded but not really needed.
rm -f web/sites/default/files/translations/*

# Scan for projects and releases.
ddev drush -y cset l10n_server.settings connectors.drupal_rest:restapi.source.restapi.refresh_url https://git.drupalcode.org/project/l10n_server/-/raw/3.0.x/assets/releases.tsv
ddev drush l10n_server:scan

# Parse a few releases.
ddev drush l10n_server:parse 'Drupal core' --release='10.0.0-beta1'
ddev drush l10n_server:parse 'Drupal core' --release='9.4.7'
# ddev drush l10n_server:parse 'Drupal core' --release='8.9.20'
# ddev drush l10n_server:parse 'Drupal core' --release='7.92'
# ddev drush l10n_server:parse 'Drupal core' --release='6.38'
# ddev drush l10n_server:parse 'Drupal core' --release='5.23'
# ddev drush l10n_server:parse 'Drupal core' --release='4.7.11'
# ddev drush l10n_server:parse 'Drupal core' --release='4.6.11'
# ddev drush l10n_server:parse 'Drupal core' --release='4.5.8'
# ddev drush l10n_server:parse 'Drupal core' --release='4.4.3'
# ddev drush l10n_server:parse 'Drupal core' --release='4.3.1'
# ddev drush l10n_server:parse 'Drupal core' --release='4.2.0'
# ddev drush l10n_server:parse 'Drupal core' --release='4.1.0'
# ddev drush l10n_server:parse 'Drupal core' --release='4.0.0'

# Add anything else.

