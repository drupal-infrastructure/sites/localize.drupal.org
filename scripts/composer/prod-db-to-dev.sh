#!/bin/bash

# IMPORTANT: Tools required
# * kubectl: https://gitlab.com/groups/drupal-infrastructure/-/wikis/kubectl-basics:-communicate-to-rancher-pods-from-local

# Bring DB from prod (or from a sanitized version of prod).
# Backups are taken each day at 00:00 so we should always have a copy of today available.

PREVIOUS_CONTEXT=$(kubectl config current-context)

# Bring copy from prod.
kubectl config use-context rancher_prod
kubectl cp api-prod/"$(kubectl get pods -n api-prod --field-selector=status.phase=Running -o custom-columns=':metadata.name' --no-headers 2>&1 | head -n 1)":/app/db_backups/db-backup-latest.sql.gz api-prod-db.sql.gz

# Move it to stage.
kubectl config use-context rancher_stage
kubectl cp api-prod-db.sql.gz api-dev/"$(kubectl get pods -n api-dev --field-selector=status.phase=Running -o custom-columns=':metadata.name' --no-headers 2>&1 | head -n 1)":/app/db_backups/db-prod-backup-latest.sql.gz

# Restore previous context.
kubectl config use-context $PREVIOUS_CONTEXT

# Remove local file.
rm api-prod-db.sql*

# Overwrite the DB and sanitize.
kubectl exec -it -n api-dev "$(kubectl get pods -n api-dev --field-selector=status.phase=Running -o custom-columns=':metadata.name' --no-headers 2>&1 | head -n 1)" -- "/app/scripts/overwrite-db-and-sanitize.sh"

