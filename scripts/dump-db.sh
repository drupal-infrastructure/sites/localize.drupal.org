#!/bin/bash

cd web && ../vendor/bin/drush sql:dump --gzip -y --result-file=/app/db_backups/db-backup-latest.sql

# Consider using this tool or similar for sanitization:
# https://www.nicksantamaria.net/post/faster-dumps-smaller-files/
