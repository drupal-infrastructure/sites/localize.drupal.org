# localize.drupal.org

D10 version of localize.drupal.org site.

## Local development with DDEV

While the following instructions assume you are using [DDEV](https://ddev.readthedocs.io/en/stable/), they can be easily updated to your specific setup.

If you haven't installed `mkcert` before, you might need to do it as a one-off.
See information [here](https://ddev.readthedocs.io/en/stable/#linux-and-macos-manual).

### Onboarding

If you prefer working on a cloud instance (Gitpod), please refer to the next section.

`git clone git@gitlab.com:drupal-infrastructure/sites/localize.git localizedrupalorg && cd localizedrupalorg`

- Start your ddev instance: `ddev start`

- To install the codebase: `ddev composer install`

- The quickest way to get a fresh site up and running is: `composer recreate`

The installation can take around 10 minutes.

You may get `block_content:uuid was not found` errors, which can be ignored. This happens because blocks captured by the Block Content module are not imported by the config system in the right order.

You may also get errors importing strings during installation. You an also ignore this, for the same reason as above. Configuration is applied only at the end of the process.

Once you have installed the project, you can use `ddev describe`, which will give you endpoints for:
* Drupal website
* Mailhog
* PhpMyAdmin or other database GUIs can be [easily added](https://ddev.readthedocs.io/en/latest/users/usage/database-management/#database-guis).

Run `ddev launch` to have a browser window popping up.

If you rather go step by step:

- To install the site: `ddev drush -y si --existing-config --account-pass=admin`

- To import basic project and release data
```
ddev drush -y cset l10n_server.settings connectors.drupal_rest:restapi.source.restapi.refresh_url https://git.drupalcode.org/project/l10n_server/-/raw/3.0.x/assets/releases.tsv
ddev drush l10n_server:scan
ddev drush l10n_server:parse 'Drupal core' --release='10.0.0-beta1'
ddev drush l10n_server:parse 'Drupal core' --release='9.4.7'
```

### Gitpod

If you prefer working on a cloud instance (Gitpod), just follow this link:
https://gitpod.io/#https://gitlab.com/drupal-infrastructure/sites/localize.

You will need an account on gitlab.com. Gitpod offers 10h a month, or 50h if you agree on linking Gitpod with a LinkedIn account.

Thanks [shaal](https://www.drupal.org/u/shaal) for working on this integration.

### Cron and queues

Queues are decoupled from cron, so we need to set up:
* `drush core:cron`
* `drush l10n_server:scan`
* `drush queue:run l10n_server_parser`
* `drush queue:run l10n_server_packager`

## Live and staging instances

### Settings

You can override settings per environment via `$settings` on the `settings.ENVIRONMENT.php` files.

### Migrations

In order to migrate data from the Drupal 7 instance:
1. Import the Drupal 7 database locally.
2. Configure access to this database in the settings.local.php file:
```
$databases['migrate']['default'] = [
  'database' => '<DATABASE>',
  'username' => '<USERNAME>',
  'password' => '<PASSWORD>',
  'host' => '<HOST>',
  'driver' => 'mysql',
  'port' => '3306',
  'prefix' => '',
];
```

3. Launch the migrations (may last for hours or even days):
```
drush mim --group=base
drush mim --group=l10n
drush mim --group=group
```
